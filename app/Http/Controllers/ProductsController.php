<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminProductRequest;
use CodeDelivery\Repositories\CategoryRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class ProductsController extends Controller
{
    public function __construct(ProductRepository $produtctRepository, CategoryRepository $categoryRepository)
    {
        $this->repository = $produtctRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index(ProductRepository $produtctRepository)
    {
        $products = $produtctRepository->paginate(5);
        return view('admin.products.index',compact('products'));
    }
    public function create()
    {
        $categories = $this->categoryRepository->lists('name','id');
        return view('admin.products.create',compact('categories'));
    }
    public function store(AdminProductRequest $request)
    {
        $data = $request->all();
        $this->repository->create($data);

        return redirect()->route('admin.products.index');
    }
    
    public function edit($id)
    {
        $product = $this->repository->find($id);

        /*
         * Foi colocado o lists pois o BaseRepository ja contem est método por padrao nao
         * podendo sobrescrever
         */
        $categories = $this->categoryRepository->lists('name','id');

        return view('admin.products.edit',compact('product','categories'));
    }


    public function update(AdminProductRequest $request, $id)
    {
        $data = $request->all();
        $this->repository->update($data,$id);

        return redirect()->route('admin.products.index');
    }

    public function destroy($id)
    {
        $this->repository->delete($id);


        return redirect()->route('admin.products.index');
    }
}
