@extends('layouts.app')

@section('content')
<div class="container">
    <h3>
        Clients
    </h3>

    <a href="{{route('admin.clients.create')}}" class="btn btn-default">Adicionar</a>
    <br/> <br/>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Telefone</th>
            <th>Ação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clients as $client)
        <tr>
            <td>{{$client->id}}</td>
            <td>{{$client->user->name}}</td>
            <td>{{$client->phone}}</td>
            <td>
                <a class="btn btn-info btn-xs" href="{{route('admin.clients.edit',$client->id)}}">Editar</a>
                <button class="btn btn-danger btn-xs">Excluir</button></td>
        </tr>
        @endforeach
        </tbody>

    </table>

    {!! $clients->render() !!}
</div>
@endsection