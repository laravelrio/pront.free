<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminClientRequest;
use CodeDelivery\Services\ClientService;
use CodeDelivery\Repositories\ClientRepository;
use CodeDelivery\Http\Requests;
use CodeDelivery\Repositories\UserRepositoryEloquent;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function __construct(ClientRepository $repository, ClientService $clientService)
    {
        $this->repository       = $repository;
        $this->clientService    = $clientService;
    }

    public function index(ClientRepository $clientRepository)
    {
        $clients = $clientRepository->paginate(5);
        return view('admin.clients.index', compact('clients'));
    }
    public function create()
    {
        return view('admin.clients.create');
    }
    public function store(AdminClientRequest $request)
    {
        $data = $request->all();
        $this->clientService->create($data);

        return redirect()->route('admin.clients.index');
    }
    
    public function edit($id)
    {
        $client = $this->repository->find($id);

        /*
         * Foi colocado o lists pois o BaseRepository ja contem est método por padrao nao
         * podendo sobrescrever
         */

        return view('admin.clients.edit',compact('client'));
    }


    /**
     * @param AdminClientRequest $request
     * @param $id
     * @return mixed
     */
    public function update(AdminClientRequest $request, $id)
    {
        $data = $request->all();
        $this->clientService->update($data,$id);


        return redirect()->route('admin.clients.index');
    }

    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('admin.clients.index');
    }
}
