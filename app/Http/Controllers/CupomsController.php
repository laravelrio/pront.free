<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminCupomRequest;
use CodeDelivery\Repositories\CupomRepository;
use CodeDelivery\Services\ClientService;
use CodeDelivery\Repositories\ClientRepository;
use CodeDelivery\Http\Requests;
use Illuminate\Http\Request;

class CupomsController extends Controller
{
    public function __construct(CupomRepository $repository)
    {
        $this->repository       = $repository;
    }

    public function index()
    {
        $cupoms = $this->repository->paginate(5);
        return view('admin.cupoms.index', compact('cupoms'));
    }

    public function create()
    {
        return view('admin.cupoms.create');
    }

    public function store(AdminCupomRequest $request)
    {

        $data = $request->all();
        $this->repository->create($data);

        return redirect()->route('admin.cupoms.index');

    }
}
